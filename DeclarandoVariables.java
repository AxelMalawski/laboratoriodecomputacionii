package labII;

public class DeclarandoVariables { 

	String nombre;
	int edad; 
	double sueldoDeseado;
	boolean carnet;  
	
	public DeclarandoVariables(String nombre, int edad, double sueldoDeseado, boolean carnet) { //  m�todo constructor. 
		this.nombre = nombre;
		this.edad = edad;
		this.sueldoDeseado = sueldoDeseado;
		this.carnet = carnet;
		
	}
	
	public static void main(String[] args) { // m�todo main
		DeclarandoVariables axel = new DeclarandoVariables("Axel", 22, 85000.53, true);  //creando el objeto que me representa. 
	
		axel.presentarse(); //metodo que presenta los datos de nombre, edad, sueldo deseado y el tener o no carnet. 	
	
	}
	
	public void presentarse() {
		System.out.println("Mi nombre es " + nombre+ ", tengo a�os " + edad + ", me gustaria tener un sueldo de: " + sueldoDeseado + "$");
		
		if(carnet == true) {
			System.out.println("Tengo carnet de conducir");
		}
		
		else{
			System.out.println("No tengo carnet de conducir");
		}

	}

}
	
	

