package labII;

import java.util.Random;

class CuentaCorriente{
	
	private double saldo;
	private String nombreTitular;
	private long numeroCuenta;
	Random randomno = new Random();
	
	
	public CuentaCorriente(String nombreTitular, double saldo) {
		
		this.nombreTitular = nombreTitular;
		this.saldo = saldo;		
		this.numeroCuenta = Math.abs(randomno.nextLong());
	
	}


	public double getSaldo() {
		return saldo;
	}

	public String getDatos() {
		return "Nombre del titular de la cuenta: " + this.nombreTitular + ", Numero de cuenta: " + this.numeroCuenta;
	}

	public void setIngreso(double saldo) {
	
		this.saldo += saldo;
	
	}

	public void setRetirar(double retiro) {
		
		if(retiro>this.saldo) {
			System.out.println("Saldo insuficiente");
		}
		
		else this.saldo -= retiro;
		
	}
	
	public void transferirDinero(CuentaCorriente destinatario, long monto) {
		
		this.setRetirar(monto);
		destinatario.setIngreso(monto);
				
	}
	
}

public class UsoCuenta {

	public static void main(String[] args) {
		
	CuentaCorriente cuenta1 = new CuentaCorriente("Axel Malawski", 55204);
	CuentaCorriente cuenta2 = new CuentaCorriente("Luis Lopez", 35000);
	
	System.out.println("Antes de la trasferencia \n");
	
	System.out.println("Saldo: " + cuenta1.getSaldo() + "$ " + cuenta1.getDatos());
	System.out.println("Saldo: " + cuenta2.getSaldo() + "$ "  + cuenta2.getDatos());
	
	cuenta1.transferirDinero(cuenta2, 2500);
	
	System.out.println("\nDespu�s de la trasferencia \n");
	
	System.out.println("Saldo: " + cuenta1.getSaldo() + "$ " + cuenta1.getDatos());
	System.out.println("Saldo: " + cuenta2.getSaldo() + "$ "  + cuenta2.getDatos());
	
	}


}
