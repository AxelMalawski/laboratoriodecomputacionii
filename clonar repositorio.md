### Clonar el repositorio a una carpeta en nuestra computadora.

1) Descargamos e instalamos git.

2) Abrimos la terminal de comandos "git bash" y procedemos a registrar nuestro usuario y correo que deben coincider con el de nuestra cuenta en GitLab. 

![alt text](https://i.ibb.co/fF7yGDV/1.png)

3) Creamos una SSH KEY.

![alt text](https://i.ibb.co/X44QrhV/2.png)

4) Vamos a donde se creó la llave copiamos el contenido del archivo cuya extension es .pub y lo pegamos en nuestra cuenta de GitLab, en  Settings - SSH Key.

![alt text](https://i.ibb.co/tXffVBq/3.png)

5) Volvemos a nuestro repositorio, y en la pestaña "clone" seleccionamos "clone with SHH" y copiamos el texto. A través de la terminal nos situamos en la carpeta donde queremos traer nuestro repositorio, y ejecutamos el comando "git clone git@gitlab.com:AxelMalawski/laboratoriodecomputacionii.git".

![alt text](https://i.ibb.co/LpmV5DD/4.png)

Ya tendriamos nuestro repositorio remoto clonado.

![alt text](https://i.ibb.co/9VmRKjQ/5.png)




