package labII;

import java.util.Scanner;
import java.lang.Math;

public class JuegoAdivinar {

	public static void main(String[] args) {
		
	Scanner entry = new Scanner(System.in);	
		
    int random = (int) Math.round(Math.random()*100);
    int numberUser;
    int attempts = 0;
    
    System.out.println("Ingrese un n�mero para tratar de adivinar");
	
    do {
			
		numberUser = entry.nextInt();
		
		if(numberUser<random) {
			
			System.out.println("El n�mero a adivinar es mayor, ingrese otro");
			attempts++;
		
		}
		
		else if(numberUser>random) {
			
			System.out.println("El n�mero a adivinar es menor, ingrese otro");
			attempts++;
		
		}
		
		
	}while(numberUser != random);
    
    entry.close();
    System.out.println("Correcto, el numero era " + random + " lo adivinaste en " + attempts + " intentos");
    
     
	}

}
