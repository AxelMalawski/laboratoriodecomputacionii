package application;

import java.util.Scanner;
import java.lang.Math;


public class RaizScanner {

	public static void main(String[] args) {
		Scanner entry = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		
		double numero = entry.nextDouble();
		
		double resultado = Math.sqrt(numero);
		
		System.out.println("La raiz de " + numero + " es " + resultado);

		entry.close();
	}

}
