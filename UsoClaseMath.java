package labII;

import java.lang.Math;
import java.util.Scanner;

public class UsoClaseMath {

	static Scanner entry = new Scanner(System.in);
	static Scanner entryDouble = new Scanner(System.in);
	public static String[] args = {};
	public static int choice = 0;
	
	public static void main(String[] args) {
			
		System.out.println("Seleccione una opci�n: \n1) Funciones trigonom�tricas.\n2) Logaritmo natural.\n3) Funcion exponencial natural.\n4) Ver valor de PI.\n5) Ver valor de E. ");
		
		
		try { 
			choice = entry.nextInt();
		}catch(Exception e) {
			entry.nextLine();
			System.out.println("Ingres� un caracter incorrecto, intentelo de nuevo");
			main(args);
		}
			
		
		switch(choice) {
			case 1: trigonometricFunctions();
					callMain();
					break;
			case 2: System.out.println("El logaritmo natural del numero seleccionado es: " + naturalLogarithm());
					callMain();
					break;
			case 3: System.out.println("El resultado de la funci�n exponencial natural del n�mero elegido es: " + euler());
					callMain();
					break;
			case 4: System.out.println("El valor de PI es " + Math.PI);
					callMain();
					break;
			case 5: System.out.println("El valor de E es " + Math.E);
					callMain();	
					break;
			default: System.out.println("No ingreso una opci�n valida, intentelo nuevamente");		
					callMain();
					break;
			}			
			
		}
	
	
	public static void trigonometricFunctions() { //metodo que se encarga del menu de las funciones trigonometricas y de mostrar el resultado;
		
		double result;
		
		System.out.println("Seleccione una opci�n: \n1) Seno\n2) Coseno.\n3) Tangente.\n4) Arcotangente\n5) Arcotangente de dos parametros ");
		
		try { 
			choice = entry.nextInt();
		}catch(Exception e) {
			entry.nextLine();
			trigonometricFunctions();
		}
		
		switch(choice) {
		case 1: result = senCosTangArctanResolver(choice);
		System.out.println("El seno del �ngulo ingresado es: " + result);
			callMain();
			break;
		case 2: result = senCosTangArctanResolver(choice);
		System.out.println("El coseno del �ngulo ingresado es: " + result);
			callMain();
			break;
	    case 3: result = senCosTangArctanResolver(choice);
		System.out.println("La tangente del �ngulo ingresado es: " + result);
			callMain();
			break;
		case 4: result = senCosTangArctanResolver(choice);
		System.out.println("El arcotangente del �ngulo ingresado es: " + result);
			callMain();
			break;
		case 5: result = senCosTangArctanResolver(choice);
		System.out.println("El arcotangente del �ngulo ingresado es: " + result);
			callMain();	
			break;
		default: System.out.println("No eligi� una opcion correcta");
			callMain();	
			break;
		}
		
		
	}

	public static double senCosTangArctanResolver(int choice) {   //funcion que pide el valor de la opcion trigonometrica seleccionada , hace la operacion y retorna el resultado. 
		
		System.out.println("Ingrese un �ngulo en radianes (sin minutos ni segundos): ");
		double DegreeValue1 = entryDouble.nextDouble();
		
		if(choice==1) return Math.sin(DegreeValue1);
		
		if(choice==2) return Math.cos(DegreeValue1);
			
		if(choice==3) return Math.tan(DegreeValue1);
			
		if(choice==4) return Math.atan(DegreeValue1);
		
		if(choice==5) {
			System.out.println("Debe ingresar otro �ngulo para usar esta opci�n");
			double DegreeValue2 = entryDouble.nextDouble();
			return Math.atan2(DegreeValue1, DegreeValue2);
		}
		
		else return 0.0;
		
		
	}
	
	
	public static double naturalLogarithm() {  //funcion logaritmo natural.
		
		double logValue = 0;
		
		do {
			System.out.println("Ingrese un valor positivo: ");
			
			logValue = entryDouble.nextDouble();
		
		}while(logValue<0);
	
		return Math.log(logValue);
	}
	
	public static double euler() { //funcion exponencial
		double eulerValue = 0;
		
		System.out.println("Ingrese un valor: ");
		
		eulerValue = entryDouble.nextDouble();
		
		return Math.exp(eulerValue); 
	}
	
	
	public static void callMain() { //metodo que pregunta al usuario si quiere continuar usando el programa para otra operacion o lo termina.
		
		int choose = 0;
		System.out.println("Queres realizar otra operaci�n?: \n1) Si.\n2) No.");
		choose = entry.nextInt();
		if(choose == 1) {
			main(args);
		}
		if(choose == 2) {
			System.exit(1);
		}

	}

}
